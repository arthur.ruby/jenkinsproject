package fr.epsi.supergestion.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Villain implements Serializable {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @NotBlank
    @Size(min = 3, max = 55)
    @Column(name = "name", nullable = false)
    private String name;

    @ManyToMany(mappedBy = "foughtVillains")
    private List<SuperHero> foughtHeroes;

    public Villain() {
        this.foughtHeroes = new ArrayList<>();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SuperHero> getFoughtHeroes() {
        return this.foughtHeroes;
    }

    public void setFoughtHeroes(List<SuperHero> foughtHeroes) {
        this.foughtHeroes = foughtHeroes;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
