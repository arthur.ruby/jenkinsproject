package fr.epsi.supergestion.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class SuperHero implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(min = 3, max = 55)
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "secrete_identity")
    @NotEmpty
    private String secreteIdentity;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(
            name = "hero_has_power",
            joinColumns = @JoinColumn(name = "hero_id"),
            inverseJoinColumns = @JoinColumn(name = "power_id")
    )
    private List<Power> powers;

    @Transient
    private Long mentorId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mentor_id")
    private SuperHero mentor;


    @OneToMany(mappedBy = "mentor", cascade = CascadeType.PERSIST)
    private List<SuperHero> sidekicks;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "nemesis_id")
    private Villain nemesis;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(
            name = "hero_has_villain",
            joinColumns = @JoinColumn(name = "hero_id"),
            inverseJoinColumns = @JoinColumn(name = "villain_id")
    )
    private List<Villain> foughtVillains;

    public SuperHero getMentor() {
        return this.mentor;
    }

    public void setMentor(SuperHero mentor) {
        this.mentor = mentor;
    }

    public SuperHero() {
        this.powers = new ArrayList<>();
        this.foughtVillains = new ArrayList<>();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecreteIdentity() {
        return this.secreteIdentity;
    }

    public void setSecreteIdentity(String secreteIdentity) {
        this.secreteIdentity = secreteIdentity;
    }

    public List<Power> getPowers() {
        return this.powers;
    }

    public void setPowers(List<Power> powers) {
        this.powers = powers;
    }

    public void addPower(Power power){
        this.powers.add(power);
    }

    public void removePower(Power power){
        this.powers.remove(power);
    }

    public List<SuperHero> getSidekicks() {
        return this.sidekicks;
    }

    public void setSidekicks(List<SuperHero> sidekicks) {
        this.sidekicks = sidekicks;
    }

    public Villain getNemesis() {
        return this.nemesis;
    }

    public void setNemesis(Villain nemesis) {
        this.nemesis = nemesis;
    }

    public List<Villain> getFoughtVillains() {
        return this.foughtVillains;
    }

    public void setFoughtVillains(List<Villain> foughtVillains) {
        this.foughtVillains = foughtVillains;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
