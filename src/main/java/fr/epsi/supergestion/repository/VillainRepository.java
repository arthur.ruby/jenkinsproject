package fr.epsi.supergestion.repository;

import fr.epsi.supergestion.model.Villain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VillainRepository extends JpaRepository<Villain, Long> {
}