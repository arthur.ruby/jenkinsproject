package fr.epsi.supergestion.repository;

import fr.epsi.supergestion.model.Power;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PowerRepository extends JpaRepository<Power, Long> {

    List<Power> findPowersByNameContains(String name);
}