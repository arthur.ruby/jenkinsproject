package fr.epsi.supergestion.api.controller;

import fr.epsi.supergestion.api.dto.PowerDto;
import fr.epsi.supergestion.model.Power;
import fr.epsi.supergestion.repository.PowerRepository;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(
        path = "/powers",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class PowerController {

    private final PowerRepository powerRepository;

    public PowerController(PowerRepository powerRepository) {
        this.powerRepository = powerRepository;
    }

    @GetMapping()
    public ResponseEntity<List<PowerDto>> getAllPowers() {
        return ResponseEntity.ok(
                this.powerRepository
                        .findAll()
                        .stream()
                        .map(this::mapToDto)
                        .collect(Collectors.toList())
        );
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<PowerDto> getOnePowerById(@PathVariable("id") Long id) {
        return this.powerRepository.findById(id)
                .map(power -> ResponseEntity.ok(mapToDto(power)))
                .orElseGet(() -> ResponseEntity.notFound().build())
                ;
    }

    @GetMapping(
            path = "/search",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<PowerDto>> getPowersByNameLike(@RequestParam("name") String name) {
        return ResponseEntity.ok(
                this.powerRepository
                        .findPowersByNameContains(name)
                        .stream()
                        .map(this::mapToDto)
                        .collect(Collectors.toList())
        );
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<PowerDto> createPower(@RequestBody PowerDto powerDto) throws URISyntaxException {
        powerDto.setId(0);
        Power power = mapToEntity(powerDto);
        return ResponseEntity
                .created(new URI("/powers/" + power.getId()))
                .body(mapToDto(powerRepository.save(power)));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Power> deletePower(@PathVariable Long id) {
        powerRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<PowerDto> updatePower(@PathVariable Long id, @RequestBody PowerDto powerDto) {
        Power updatedPower = mapToEntity(powerDto);
        return this.powerRepository.findById(id)
                .map(power -> {
                            power.setName(updatedPower.getName());
                            power.setDescription(updatedPower.getDescription());
                            return ResponseEntity
                                    .ok()
                                    .body(mapToDto(this.powerRepository.save(power)));
                        }
                )
                .orElseGet(() -> ResponseEntity.notFound().build())
                ;
    }

    private PowerDto mapToDto(Power power) {
        return new PowerDto(
                power.getId(),
                power.getName(),
                power.getDescription()
        );
    }

    private Power mapToEntity(PowerDto powerDto) {
        Power power = new Power();
        power.setId(powerDto.getId());
        power.setName(powerDto.getName());
        power.setDescription(powerDto.getDescription());
        return power;
    }
}
