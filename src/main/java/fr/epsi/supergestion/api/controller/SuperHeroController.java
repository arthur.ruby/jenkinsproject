package fr.epsi.supergestion.api.controller;

import fr.epsi.supergestion.api.dto.SuperHeroDto;
import fr.epsi.supergestion.model.SuperHero;
import fr.epsi.supergestion.repository.SuperHeroRepository;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(
        path = "/superheroes",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class SuperHeroController {

    private final SuperHeroRepository superHeroRepository;

    public SuperHeroController(SuperHeroRepository superHeroRepository) {
        this.superHeroRepository = superHeroRepository;
    }

    @GetMapping()
    public ResponseEntity<List<SuperHeroDto>> getAllSuperHeroes() {
        return ResponseEntity.ok(
                this.superHeroRepository
                        .findAll()
                        .stream()
                        .map(this::mapToDto)
                        .collect(Collectors.toList())
        );
    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<SuperHeroDto> getOneSuperHeroById(@PathVariable("id") Long id) {
        return this.superHeroRepository.findById(id)
                .map(superHero -> ResponseEntity.ok(mapToDto(superHero)))
                .orElseGet(() -> ResponseEntity.notFound().build())
                ;
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<SuperHeroDto> createSuperHero(@RequestBody SuperHeroDto superHeroDto) throws URISyntaxException {
        superHeroDto.setId(0);
        SuperHero superHero = mapToEntity(superHeroDto);
        return ResponseEntity
                .created(new URI("/superheroes/" + superHero.getId()))
                .body(mapToDto(superHeroRepository.save(superHero)));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<SuperHero> killSuperHero(@PathVariable Long id) {
        superHeroRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping(
            value = "/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<SuperHeroDto> updateSuperHero(@PathVariable Long id, @RequestBody SuperHeroDto superHeroDto) {
        SuperHero updatedHero = mapToEntity(superHeroDto);
        return this.superHeroRepository.findById(id)
                .map(superHero -> {
                            superHero.setName(updatedHero.getName());
                            superHero.setSecreteIdentity(updatedHero.getSecreteIdentity());
                            return ResponseEntity
                                    .ok()
                                    .body(mapToDto(this.superHeroRepository.save(superHero)));
                        }
                )
                .orElseGet(() -> ResponseEntity.notFound().build())
                ;
    }

    private SuperHeroDto mapToDto(SuperHero superHero) {
        return new SuperHeroDto(
                superHero.getId(),
                superHero.getName(),
                superHero.getSecreteIdentity()
        );
    }

    private SuperHero mapToEntity(SuperHeroDto superHeroDto) {
        SuperHero superHero = new SuperHero();
        superHero.setId(superHeroDto.getId());
        superHero.setName(superHeroDto.getName());
        superHero.setSecreteIdentity(superHeroDto.getSecretIdentity());
        return superHero;
    }
}
