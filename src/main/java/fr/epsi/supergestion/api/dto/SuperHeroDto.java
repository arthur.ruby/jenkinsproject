package fr.epsi.supergestion.api.dto;

public class SuperHeroDto {
    private long id;
    private String name;
    private String secretIdentity;

    public SuperHeroDto() {
    }

    public SuperHeroDto(long id, String name, String secretIdentity) {
        this.id = id;
        this.name = name;
        this.secretIdentity = secretIdentity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecretIdentity() {
        return secretIdentity;
    }

    public void setSecretIdentity(String secretIdentity) {
        this.secretIdentity = secretIdentity;
    }
}
