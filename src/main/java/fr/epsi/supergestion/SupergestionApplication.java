package fr.epsi.supergestion;

import fr.epsi.supergestion.model.Power;
import fr.epsi.supergestion.model.SuperHero;
import fr.epsi.supergestion.model.Villain;
import fr.epsi.supergestion.repository.PowerRepository;
import fr.epsi.supergestion.repository.SuperHeroRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupergestionApplication {

    private final PowerRepository powerRepository;

    private final SuperHeroRepository superHeroRepository;

    public SupergestionApplication(PowerRepository powerRepository, SuperHeroRepository superHeroRepository) {
        this.powerRepository = powerRepository;
        this.superHeroRepository = superHeroRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(SupergestionApplication.class, args);
    }

//    @PostConstruct
    public void initData() {
        Power power = new Power();
        power.setName("Swimming");
        power.setDescription("The ability to swim");
        power = powerRepository.save(power);

        SuperHero aquaman = new SuperHero();
        aquaman.setName("Aquaman");
        aquaman.addPower(power);
        aquaman = superHeroRepository.save(aquaman);
        aquaman.setNemesis(new Villain());
    }
}
